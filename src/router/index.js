import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [{
            path: "/",
            redirect: "/product"
        }, {
            path: "/cart",
            component: () =>
                import ("@/views/cart/index.vue")
        }, {
            path: "/product",
            redirect: "/product/list",
            component: () =>
                import ("@/views/product/index.vue"),
            children: [{
                path: "list",
                component: () =>
                    import ("@/views/product/children/list.vue")
            }, {
                path: "detail",
                component: () =>
                    import ("@/views/product/children/detail.vue")
            }]
        },
        {
            path: "/type",
            component: () =>
                import ("@/views/type/index.vue")
        },
        {
            path: "/order",
            redirect: "/order/list",
            component: () =>
                import ("@/views/order/index.vue"),
            children: [{
                path: "list",
                component: () =>
                    import ("@/views/order/children/list.vue")
            }, {
                path: "detail",
                component: () =>
                    import ("@/views/order/children/detail.vue")
            }]
        }, {
            path: "/my",
            redirect: "/my/person",
            component: () =>
                import ("@/views/my/index.vue"),
            children: [{
                path: "person",
                component: () =>
                    import ("@/views/my/children/person.vue")
            }, {
                path: "set",
                component: () =>
                    import ("@/views/my/children/set.vue")
            }]
        }, {
            path: "/account",
            redirect: "/account/login",
            component: () =>
                import ("@/views/account/index.vue"),
            children: [{
                path: "login",
                component: () =>
                    import ("@/views/account/children/login.vue")
            }, {
                path: "register",
                component: () =>
                    import ("@/views/account/children/register.vue")
            }]
        }, {
            path: "/demo",
            component: () =>
                import ("@/views/demo/index.vue")
        }, {
            path: "/demo/test",
            component: () =>
                import ("@/views/demo/test.vue")
        }, {
            path: "/demo/test01",
            component: () =>
                import ("@/views/demo/test01.vue")
        }
    ]
})