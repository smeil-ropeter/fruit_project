import Vue from 'vue'
import App from './App.vue'
import Vant from 'vant';
import 'vant/lib/index.css';
import '@/assets/style/reset.less';
import '@/assets/style/common.less';
import router from './router/index.js';
import store from './store/index.js';

Vue.use(Vant);

Vue.config.productionTip = false

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app')