import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        token: '',
        username: '路飞',
        age: 18,
        phone: '',
        num: 20
    },
    getters: {
        sum() {
            return age + num;
        }
    },
    mutations: {
        // setUsername(state, payload) {
        //     state.username = payload;
        // },
        // // 定义修改age的mutation
        // setAge(state, payload) {
        //     state.age = payload;
        // },
        setToken(state, payload) {
            state.token = payload;
        },
        setPhone(state, payload) {
            state.phone = payload;
        }
    }
});

export default store;